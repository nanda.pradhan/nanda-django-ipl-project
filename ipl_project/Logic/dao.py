from ipl_project.models import matches,deliveries
from django.db.models import Count,Sum,F,Q
from ipl_project.Logic import convert_to_list
from copy import deepcopy



def match_played_per_year():
    matches_played_count=matches.objects.values('season').annotate(dcount=Count('season'))\
    .order_by('season')
    years=[]
    matches_count=[]
    for data in matches_played_count:
        years.append(data['season'])
        matches_count.append(data['dcount'])
    return years,matches_count

def extra_runs_per_team(year):
    extra_runs=deliveries.objects.filter(match_id__season=year).values('bowling_team')\
    .annotate(sum=Sum('extra_runs')).order_by('bowling_team')
    result_dict={}
    for data in extra_runs:
        result_dict.update({data['bowling_team']:data['sum']})
    return result_dict

    
def matches_win_per_year():
    year_wise_team_win_count=matches.objects.filter(~Q(winner="")).values('season','winner')\
    .annotate(win_count=Count('winner')).order_by('season')
    teams = list()
    years = list()
    for team in year_wise_team_win_count:
        if team['winner'] not in teams:
            teams.append(team['winner'])
        if team['season'] not in years:
            years.append(team['season'])

    team_counter = dict((team, 0) for team in teams)
    matches_won_per_year = dict()
    for year in years:
        matches_won_per_year[year] = deepcopy(team_counter)
    for team in year_wise_team_win_count:
        matches_won_per_year[team['season']][team['winner']] = team['win_count']
    return matches_won_per_year
           
  

def economical_bowler(year):
    eco_bowler=deliveries.objects.filter(match_id__season=year).values('bowler')\
    .annotate(sum_run=Sum('total_runs'),sum_legby=Sum('legby_runs'),sum_ball=Count('bowler', fliter=(Q(wide_Run=0,noball_run=0))))
    economical_bowler_dict={}
    for data in eco_bowler:
        eco_score=(data['sum_run']-data['sum_legby'])/data['sum_ball']
        economical_bowler_dict.update({data['bowler']:eco_score})
    return economical_bowler_dict
    


def max_man_of_the_match():
    man_of_match=matches.objects.values('player_of_match').annotate(count=Count('player_of_match'))
    man_of_the_match_count={}
    for data in man_of_match:
        man_of_the_match_count.update({data['player_of_match']:data['count']})
    return man_of_the_match_count

    
    
    



    


    
    




    
    
    
    
