from ipl_project import models
from ipl_project.models import matches,deliveries
import csv
from django.db import transaction

@transaction.atomic
def insert_data():
    reader = csv.DictReader(open('/home/dev/django_project/ipl_project/matches.csv', 'r'))
    dict_list = []
    for line in reader:
        dict_list.append(dict(line))
       
    for data in dict_list:
        obj=matches(
            id=data['id'],
            season=data['season'],
            city=data['city'],
            date=data['date'],
            team1=data['team1'],
            team2=data['team2'],
            toss_winner=data['toss_winner'],
            toss_decision=data['toss_decision'],
            result=data['result'],
            dl_applied=data['dl_applied'],
            winner=data['winner'],
            win_by_run=data['win_by_runs'],
            win_by_wickets=data['win_by_wickets'],
            player_of_match=data['player_of_match'],
            venue=data['venue'],
            umpire1=data['umpire1'],
            umpire2=data['umpire2'])
        obj.save()
@transaction.atomic
def insert_data1():
    reader = csv.DictReader(open('/home/dev/django_project/ipl_project/deliveries.csv', 'r'))
    dict_list = []
    for line in reader:
        dict_list.append(dict(line))
    for data in dict_list:
        obj=deliveries(
        innings=data['inning'],
        batting_team=data['batting_team'],
        bowling_team=data['bowling_team'],
        over=data['over'],
        ball=data['ball'],
        batsman=data['batsman'],
        non_striker=data['non_striker'],
        bowler=data['bowler'],
        is_super_over=data['is_super_over'],
        wide_rund=data['wide_runs'],
        bye_runs=data['bye_runs'],
        legby_runs=data['legbye_runs'],
        noball_runs=data['noball_runs'],
        panalty_runs=data['penalty_runs'],
        batsman_runs=data['batsman_runs'],
        extra_runs=data['extra_runs'],
        total_runs=data['total_runs'],
        player_dismissed=data['player_dismissed'],
        dismissied_kind=data['dismissal_kind'],
        fielder=data['fielder'],
        match_id=matches.objects.get(pk=data['match_id']))
        obj.save()
            

    
                
