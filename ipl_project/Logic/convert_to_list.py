
def dict_to_list_sort_on_values_dsc(data):
    keys=[]
    values=[]
    for key in sorted(data.keys(), key=data.get,reverse=True):
        keys.append(key)
        values.append(data[key])
    return keys,values
def dict_to_list_sort_on_values_asc(data):
    keys=[]
    values=[]
    for key in sorted(data.keys(), key=data.get):
        keys.append(key)
        values.append(data[key])
    return keys,values

def dict_to_key_value(data):
    years=[]
    win_count={}
    for year in sorted(data):
        years.append(year)
        for team_wise in sorted(data[year]):
            if team_wise in win_count:
                 win_count[team_wise].append(data[year][team_wise])
              
            else:
                win_count[team_wise]=[data[year][team_wise]]
    return years,win_count

