from django.http import HttpResponse
from ipl_project.Logic import database_logic
from ipl_project.Logic import dao
from ipl_project.Logic import convert_to_list
from django.template import loader
from django.shortcuts import render

def index_page(request):
    template=loader.get_template('ipl_project/index.html')
    context={}
    return HttpResponse(template.render(context,request))

def matches_per_year(request):
    years,matches_count=dao.match_played_per_year()
    chart = {
     'chart': {'type': 'column'},
     'title': {'text': 'Matches Played Per Year'},
     'xAxis':{'categories': years},
     'series':[{'name':'Year','data':matches_count,'color':'mediumseagreen'}]
        }
    return render(request,'ipl_project/plotbar.html',context={'chart':chart})

def matches_win_per_year(request):
    data=dao.matches_win_per_year()
    years,win_count=convert_to_list.dict_to_key_value(data)
    values_data=list()
    color_values=0
    get_color = ['blue', 'green', 'orange', 'black', 'purple', 'pink', 'darkgray', 'crimson', 'blueviolet', 'indigo', 'khaki',
        'brown', 'lawngreen', 'teal']
    for key in sorted(win_count.keys()):
        values_data.append({'color':get_color[color_values], 'name':key, 'data':win_count[key]})
        color_values+=1
    chart  = {
        'chart': {'type': 'column'},
        'title': {'text': 'Matches win per year'},
        'plotOptions':{'series':{'stacking':'normal'}},
        'xAxis':{'categories': years,'name':'matches'},
        'series':values_data
       }    
   
    return render(request,'ipl_project/plotbar.html',context={'chart':chart})
               

def extra_run(request):
    #third questions--3
    extra_runs=dao.extra_runs_per_team(2016)
    keys,values=convert_to_list.dict_to_list_sort_on_values_dsc(extra_runs)
    chart = {
     'chart': {'type': 'column'},
     'title': {'text': 'Extra run given by team in 2016'},
     'xAxis':{'categories': keys},
     'series':[{'name':'Team Name','data':values,'color':'red'}]
        }
    return render(request,'ipl_project/plotbar.html',context={'chart':chart})
    

def economical_bowler(request):
    economical_baller=dao.economical_bowler(2015)
    keys,values=convert_to_list.dict_to_list_sort_on_values_asc(economical_baller)
    chart = {
     'chart': {'type': 'column'},
     'title': {'text': 'Economical Bowler of 2015'},
     'xAxis':{'categories': keys[:10]},
     'series':[{'name':'Bowler Name','data':values[:10],'color':'blue'}]
        }
    return render(request,'ipl_project/plotbar.html',context={'chart':chart})
    

def story(request):
    man_of_the_match_count=dao.max_man_of_the_match()
    keys,values=convert_to_list.dict_to_list_sort_on_values_dsc(man_of_the_match_count)
    chart = {
     'chart': {'type': 'column'},
     'title': {'text': 'Max Man Of The Matches Won'},
     'xAxis':{'categories': keys[:10]},
     'series':[{'name':'player name','data':values[:10],'color':'darkorchid'}]
        }
    return render(request,'ipl_project/plotbar.html',context={'chart':chart})
    
    



    
    
    
    
    

     
    
