# Generated by Django 2.0.5 on 2018-05-24 14:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ipl_project', '0003_auto_20180524_1040'),
    ]

    operations = [
        migrations.AlterField(
            model_name='deliveries',
            name='is_super_over',
            field=models.IntegerField(),
        ),
    ]
