from django.urls import path
from . import views

urlpatterns = [
    path('index/', views.index_page,name='Index_page'),
    path('first/', views.matches_per_year,name='matches_per_year'),
    path('second/', views.matches_win_per_year,name='matches_played_per_year'),
    path('third/', views.extra_run,name='index_page'),
    path('fourth/', views.economical_bowler,name='economical_bowler'),
    path('story/', views.story,name='index_page'),
]